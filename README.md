# pip2 (for Python)

This Ansible role installs [Pip2](https://pip.pypa.io) on Ubuntu/Debian.

This role was adapted from [geerlingguy.pip](https://github.com/geerlingguy/ansible-role-pip).
